// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.


import 'package:flutter_test/flutter_test.dart';

import 'package:flutter_questionnaire/answer_button.dart';
import 'package:flutter_questionnaire/main.dart';

void main() {
    group("QuestionnaireTestingWidget", () {
        testWidgets('Finding options and clicking the option test.', (WidgetTester tester) async {
            // Load the widget.
            await tester.pumpWidget(App());

            // Define all finders.
            final businessFinder = find.widgetWithText(AnswerButton, "Asset Management");
            final sizeFinder = find.widgetWithText(AnswerButton, "More than 10,000");
            final regionFinder = find.widgetWithText(AnswerButton, "Europe");
            final durationFinder = find.widgetWithText(AnswerButton, "6-9 months");

            // Execute actual tests to find options and click it. Use tester.pump() to trigger UI update after clicking the option.
            expect(businessFinder, findsOneWidget);
            await tester.tap(businessFinder);
            await tester.pump();

            expect(sizeFinder, findsOneWidget);
            await tester.tap(sizeFinder);
            await tester.pump();

            expect(regionFinder, findsOneWidget);
            await tester.tap(regionFinder);
            await tester.pump();
            
            expect(durationFinder, findsOneWidget);
            await tester.tap(durationFinder);
            await tester.pump();

            // Test the results of the questionnare by finding the question and answer texts.
            List results = [
                {
                    'question': 'What is the nature of your business needs?',
                    'answer': 'Asset Management'
                }, 
                {
                    'question': 'What is the expected size of the user base?',
                    'answer': 'More than 10,000'
                },
                {
                    'question': 'In which region would the majority of the user base be?',
                    'answer': 'Europe'    
                },
                {
                    'question': 'What is the expected project duration?',
                    'answer': '6-9 months'
                }
            ];

            results.forEach((result) {
                final questionFinder = find.text(result['question']);
                final answerFinder = find.text(result['answer']);

                expect(questionFinder, findsOneWidget);
                expect(answerFinder, findsOneWidget);
            });

        });
    });
}
